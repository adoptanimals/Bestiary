# Bestiary
Backend services, and related infrastructure, that power [Adopt Animals](https://adoptanimals.io).

## Code License
The source code of this project is licensed under the terms of the ISC license, listed in the [LICENSE](LICENSE.md) file. A concise summary of the ISC license is available at [choosealicense.org](http://choosealicense.com/licenses/isc/).

