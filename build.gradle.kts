import org.gradle.jvm.tasks.Jar
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension
import io.spring.gradle.dependencymanagement.dsl.ImportsHandler
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.api.tasks.testing.Test
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val bestiaryVersion by project
val kotlinVersion by project

val projectTitle = "Bestiary"

buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("io.spring.gradle:dependency-management-plugin:1.0.3.RELEASE")
    }
}

apply {
    plugin("jacoco")
    plugin("idea")
    plugin("io.spring.dependency-management")
}

configure<DependencyManagementExtension> {
    imports {
        mavenBom("com.amazonaws:aws-java-sdk-bom:1.11.228")
    }
}

plugins {
    java
    kotlin("jvm") version "1.2.21"
    kotlin("kapt") version "1.2.21"
    id("com.github.johnrengelman.shadow") version "2.0.2"
}

jacoco {
    toolVersion = "0.7.9"
}

val jacocoTestReport = project.tasks.getByName("jacocoTestReport")

jacocoTestReport.doFirst {
    (jacocoTestReport as JacocoReport).classDirectories = fileTree("build/classes/kotlin/main").apply {
        // Exclude well known data classes that should contain no logic
        // Remember to change values in codecov.yml too
        exclude("**/*Event.*")
        exclude("**/*State.*")
        exclude("**/*Configuration.*")
        exclude("**/*Runner.*")
        exclude("**/*Factory.*")
        exclude("**/*Sleeper.*")
        exclude("**/*Wrapper.*")
        exclude("**/Bestiary.kt")
    }

    jacocoTestReport.reports.xml.isEnabled = true
    jacocoTestReport.reports.html.isEnabled = true
}

tasks.withType<KotlinCompile> {
    sourceCompatibility = JavaVersion.VERSION_1_9.toString()
    targetCompatibility = JavaVersion.VERSION_1_9.toString()

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

repositories {
    mavenCentral()
}

dependencies {
    // common:

    implementation(kotlin("stdlib", kotlinVersion as String))
    implementation(kotlin("reflect", kotlinVersion as String))

    implementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("com.squareup.okio:okio:1.14.0")
    implementation("io.reactivex.rxjava2:rxjava:2.1.6")
    implementation("io.reactivex.rxjava2:rxkotlin:2.1.0")

    implementation("com.squareup.okhttp3:okhttp:3.9.1")
    implementation("com.squareup.moshi:moshi:1.5.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.5.0")

    implementation("javax.xml.bind:jaxb-api:2.2.8")
    implementation("javax.activation:activation:1.1.1")

    implementation("com.fasterxml.jackson.core:jackson-core:2.9.4")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.9.4")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.9.4")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.4")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.9.4")

    implementation("io.dropwizard:dropwizard-core:1.2.4") {
        exclude("ch.qos.logback", "logback-classic")
        exclude("ch.qos.logback", "logback-access")
        exclude("org.slf4j", "log4j-over-slf4j")
    }

    implementation("io.dropwizard:dropwizard-http2:1.2.4")

    // aggregator:

    implementation("com.amazonaws:aws-java-sdk-s3")

    // scheduler:
    implementation("org.quartz-scheduler:quartz:2.2.1")
    implementation("org.quartz-scheduler:quartz-jobs:2.2.1")

    // tests:

    testImplementation("junit:junit:4.12")
    testImplementation("org.mockito:mockito-core:2.2.9")
    testImplementation("com.nhaarman:mockito-kotlin:1.5.0")
}

test {
    testLogging.setEvents(listOf("passed", "skipped", "failed", "standardError"))

    outputs.upToDateWhen { false }

    workingDir = File("testRun")
}

task<Test>("aggregatorTest") {
    testLogging.setEvents(listOf("passed", "skipped", "failed", "standardError"))

    filter {
        includeTestsMatching("animals.adopt.bestiary.aggregator.*")
    }

    outputs.upToDateWhen { false }
}

task<Test>("schedulerTest") {
    testLogging.setEvents(listOf("passed", "skipped", "failed", "standardError"))

    filter {
        includeTestsMatching("animals.adopt.bestiary.scheduler.*")
    }

    outputs.upToDateWhen { false }
}

val buildNumberAddition = if(project.hasProperty("BUILD_NUMBER")) { ".${project.property("BUILD_NUMBER")}" } else { "" }
val branchAddition = if(project.hasProperty("BRANCH")) {
    val safeBranchName = project.property("BRANCH")
            .toString()
            .map { if(Character.isJavaIdentifierPart(it)) it else '_' }
            .joinToString(separator = "")

    when (safeBranchName) {
        "develop" -> ""
        else -> "-$safeBranchName"
    }
} else {
    ""
}

version = "$bestiaryVersion$buildNumberAddition$branchAddition"
group = "animals.adopt.bestiary"
project.setProperty("archivesBaseName", projectTitle)

shadowJar {
    mergeServiceFiles()
    exclude("META-INF/*.DSA")
    exclude("META-INF/*.RSA")
}

val sourcesTask = task<Jar>("sourcesJar") {
    dependsOn("classes")

    from(sourceSets("main").allSource)
    classifier = "sources"
}

project.artifacts.add("archives", sourcesTask)
project.artifacts.add("archives", shadowJarTask())

fun Project.jar(setup: Jar.() -> Unit) = (project.tasks.getByName("jar") as Jar).setup()
fun jacoco(setup: JacocoPluginExtension.() -> Unit) = the<JacocoPluginExtension>().setup()
fun shadowJar(setup: ShadowJar.() -> Unit) = shadowJarTask().setup()
fun Project.test(setup: Test.() -> Unit) = (project.tasks.getByName("test") as Test).setup()
fun Project.compileJava(setup: JavaCompile.() -> Unit) = (project.tasks.getByName("compileJava") as JavaCompile).setup()
fun shadowJarTask() = (project.tasks.findByName("shadowJar") as ShadowJar)
fun sourceSets(name: String) = (project.property("sourceSets") as SourceSetContainer).getByName(name)
