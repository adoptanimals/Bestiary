package animals.adopt.bestiary.api.v1

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/v1/animals")
@Produces(MediaType.APPLICATION_JSON)
class AnimalsResource {

    @GET
    fun getAnimals(): Animals {
        return Animals(animals = listOf(Animal(id = "hello"), Animal(id = "world")))
    }

    data class Animals(val animals: List<Animal>)
    data class Animal(val id: String)

}