package animals.adopt.bestiary.api

import animals.adopt.bestiary.api.v1.AnimalsResource
import animals.adopt.bestiary.common.helper.loggerFor
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment

object BestiaryApi {

    @JvmStatic fun main(args: Array<String>) {
        Application().run(*args)
    }

    data class Configuration(val something: String): io.dropwizard.Configuration()

    class Application: io.dropwizard.Application<Configuration>() {
        private val logger = loggerFor<Application>()

        override fun getName(): String {
            return "bestiary-api"
        }

        override fun initialize(bootstrap: Bootstrap<Configuration>) {
            logger.info("initialising...")

            bootstrap.objectMapper.registerKotlinModule()
        }

        override fun run(configuration: Configuration, environment: Environment) {
            logger.info("hello, world!")

            val animalsResource = AnimalsResource()

            environment.jersey().register(animalsResource)
        }

    }

}