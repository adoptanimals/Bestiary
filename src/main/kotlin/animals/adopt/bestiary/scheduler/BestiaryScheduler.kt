package animals.adopt.bestiary.scheduler

import animals.adopt.bestiary.common.helper.loggerFor
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import org.quartz.JobBuilder
import org.quartz.SchedulerFactory
import org.quartz.SimpleScheduleBuilder
import org.quartz.TriggerBuilder
import org.quartz.impl.StdSchedulerFactory

object BestiaryScheduler {

    @JvmStatic fun main(args: Array<String>) {
        BestiaryScheduler.Application().run(*args)
    }

    data class UrlPingJobConfig(val name: String, val url: String, val intervalSeconds: Int)
    data class Configuration(val jobs: List<UrlPingJobConfig>): io.dropwizard.Configuration()

    class Application: io.dropwizard.Application<Configuration>() {
        private val logger = loggerFor<Application>()

        private lateinit var schedulerFactory: SchedulerFactory

        override fun getName(): String {
            return "bestiary-scheduler"
        }

        override fun bootstrapLogging() {}

        override fun initialize(bootstrap: Bootstrap<Configuration>) {
            logger.info("initialising...")

            bootstrap.objectMapper.registerKotlinModule()

            bootstrap.configurationSourceProvider = SubstitutingSourceProvider(
                bootstrap.configurationSourceProvider,
                EnvironmentVariableSubstitutor()
            )

            schedulerFactory = StdSchedulerFactory()
        }

        override fun run(configuration: Configuration, environment: Environment) {
            logger.info("hello, world")

            logger.info("Configured to run jobs: ${configuration.jobs}")

            val scheduler = schedulerFactory.scheduler

            for (configuredJob in configuration.jobs) {
                val job = JobBuilder.newJob()
                    .ofType(UrlPingJob::class.java)
                    .withIdentity("ping-${configuredJob.name}-job")
                    .usingJobData("url", configuredJob.url)
                    .build()
                val trigger = TriggerBuilder.newTrigger().withIdentity("ping-${configuredJob.name}-trigger")
                    .startNow()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(configuredJob.intervalSeconds).repeatForever())
                    .build()

                scheduler.scheduleJob(job, trigger)
            }

            scheduler.start()
        }
    }
}