package animals.adopt.bestiary.scheduler

import animals.adopt.bestiary.common.helper.OkHttpExecuting
import animals.adopt.bestiary.common.helper.loggerFor
import io.reactivex.rxkotlin.blockingSubscribeBy
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.quartz.Job
import org.quartz.JobExecutionContext

class UrlPingJob : Job {

    private val logger = loggerFor<UrlPingJob>()
    private val client = OkHttpClient()

    override fun execute(context: JobExecutionContext) {
        val url = context.mergedJobDataMap["url"] as? String
        if (url == null) {
            logger.error("not running because url was not passed in")
            return
        }

        val request = Request.Builder().url(url).cacheControl(CacheControl.FORCE_NETWORK).build()

        OkHttpExecuting.execute(request, client)
            .blockingSubscribeBy(onError = {
               logger.error("failed to run job: ${it.localizedMessage}")
            }, onNext = {
                if (it.isSuccessful) {
                    logger.info("successful ping $it")
                } else {
                    logger.warn("non-successful ping $it")
                }
            })
    }

}