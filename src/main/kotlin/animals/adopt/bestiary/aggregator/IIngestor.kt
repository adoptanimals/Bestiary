package animals.adopt.bestiary.aggregator

import com.amazonaws.services.s3.AmazonS3
import java.io.File

interface IIngestor {
    val name: String
    fun ingest(jobDirectory: File, s3Bucket: String, s3Client: AmazonS3)
}