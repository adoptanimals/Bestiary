package animals.adopt.bestiary.aggregator.helper

import java.util.regex.Pattern

object Regexes {
    val files = Pattern.compile("^[a-zA-Z0-9_.]*$").asPredicate()
    val numeric = Pattern.compile("^[0-9]*$").asPredicate()
}