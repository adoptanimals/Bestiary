package animals.adopt.bestiary.aggregator.helper

import animals.adopt.bestiary.common.helper.OkHttpExecuting.execute
import animals.adopt.bestiary.common.helper.loggerFor
import com.amazonaws.services.s3.AmazonS3
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.HashingSource
import okio.Okio
import java.io.File

object Helper {

    private val logger = loggerFor<Helper>()
    
    const val bytesInMegabyte = 1000000

    fun doWeHaveLatestAlready(client: OkHttpClient, bucketName: String, s3client: AmazonS3, moshi: Moshi, prefix: String, filename: String, url: String): Boolean {
        val metadata = s3client.getObject(bucketName, "$prefix/$filename.metadata.json").use {
            try {
                val s3meta = s3client.getObject(bucketName, "$prefix/$filename.metadata.json")
                val data = Okio.buffer(Okio.source(s3meta.objectContent))

                return@use moshi.adapter(FileMetadata::class.java).fromJson(data)
            } catch (exception: Exception) {
                return@use null
            }
        } ?: return false

        val headRequest = Request.Builder().url(url).method("HEAD", null).build()
        return execute(headRequest, client)
            .map {
                val body = it.body()
                if (!it.isSuccessful || body == null) {
                    return@map false
                }

                val headers = it.headers()

                val lastModified = headers["Last-Modified"]
                val contentLength = headers["Content-Length"]?.toLong() ?: 0

                val downloadedMeta = FileMetadata(
                    url,
                    lastModified,
                    contentLength,
                    sha2 = null
                )

                logger.debug("Web meta: $downloadedMeta")
                logger.debug("Local meta: $metadata")

                return@map metadata.equalsWithoutHash(downloadedMeta)
            }
            .blockingFirst()
    }

    fun clearAndRemakeDirectory(dir: File) {
        if (dir.exists()) {
            val deletedEverything = dir.deleteRecursively()
            if (!deletedEverything) {
                throw RuntimeException("Failed to delete directory $dir")
            }
        }

        val madeDir = dir.mkdir()
        if (!madeDir) {
            throw RuntimeException("Failed to create directory $dir")
        }
    }

    fun latestDirectory(baseDir: File): File? {
        return baseDir
            .listFiles { dir, name ->
                dir.isDirectory && name.length >= 10 && Regexes.numeric.test(name)
            }
            .sortedWith(compareBy(File::getName))
            .lastOrNull()
    }

    data class DownloadedFile(val file: File, val metadata: FileMetadata)

    fun downloadFile(url: String, client: OkHttpClient, moshi: Moshi, expectedContentType: String, sizeLimitBytes: Long, downloadFolder: File, downloadFilename: String): Observable<DownloadedFile> {
        val request = Request.Builder().url(url).build()

        val downloadFile = File(downloadFolder, downloadFilename)
        logger.debug("Downloading $url to $downloadFile")

        return execute(request, client)
            .flatMap {
                val body = it.body()

                if (it.isSuccessful && body != null) {
                    val contentLength = body.contentLength()
                    if (contentLength <= 0 || contentLength > sizeLimitBytes) {
                        throw RuntimeException("Unexpected content size $contentLength")
                    }

                    val contentType = body.contentType()
                    if (contentType != okhttp3.MediaType.parse(expectedContentType)) {
                        throw RuntimeException("Expected content type $expectedContentType, got $contentType")
                    }

                    val lastModified = it.header("Last-Modified")
                    val sha2 = HashingSource.sha256(body.source()).hash().base64()
                    val metadata = FileMetadata(
                        url,
                        lastModified,
                        contentLength,
                        sha2
                    )
                    val exportMetaFile = File(downloadFolder, "$downloadFilename.metadata.json")
                    writeJsonFile(moshi, metadata, exportMetaFile)

                    val sink = Okio.buffer(Okio.sink(downloadFile))
                    sink.writeAll(body.source())
                    sink.close()

                    logger.debug("Wrote file (and metadata) to $downloadFile")

                    return@flatMap Observable.just(
                        DownloadedFile(
                            downloadFile,
                            metadata
                        )
                    )
                } else {
                    throw RuntimeException("File download failed: $it")
                }
            }
    }

    inline fun <reified T> readJsonFile(moshi: Moshi, file: File): T? {
        val mapper = moshi.adapter(T::class.java)
        return try {
            mapper.fromJson(Okio.buffer(Okio.source(file)))
        } catch (exception: Exception) {
            null
        }
    }

    inline fun <reified T> writeJsonFile(moshi: Moshi, contents: T, file: File): Boolean {
        val mapper = moshi.adapter(T::class.java)
        return try {
            val sink = Okio.buffer(Okio.sink(file))
            mapper.toJson(sink, contents)
            sink.close()
            true
        } catch (exception: Exception) {
            false
        }
    }

    inline fun <reified T> readXmlFile(mapper: XmlMapper, file: File): T? {
        val typeRef = object: TypeReference<T>() {}
        return mapper.readValue(file, typeRef)
    }

    data class FileMetadata(val source: String, val lastModified: String?, val contentLength: Long?, val sha2: String?) {

        fun equalsWithoutHash(other: Any?): Boolean {
            if (this === other) return true
            if (other !is FileMetadata) return false

            if (source != other.source) return false
            if (lastModified != other.lastModified) return false
            if (contentLength != other.contentLength) return false

            return true
        }

    }

}