package animals.adopt.bestiary.aggregator

import animals.adopt.bestiary.aggregator.anilog.ingest.AnilogIngest
import animals.adopt.bestiary.aggregator.api.ingest.IngestHealthCheck
import animals.adopt.bestiary.aggregator.api.ingest.IngestJobManager
import animals.adopt.bestiary.aggregator.api.ingest.IngestResource
import animals.adopt.bestiary.aggregator.api.transform.TransformHealthCheck
import animals.adopt.bestiary.aggregator.api.transform.TransformResource
import animals.adopt.bestiary.common.helper.loggerFor
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import okhttp3.OkHttpClient

object BestiaryAggregator {

    @JvmStatic fun main(args: Array<String>) {
        Application().run(*args)
    }

    data class IngestorConfig(val name: String, val type: String, val baseUrl: String)
    data class IngestorsConfig(val ingestors: List<IngestorConfig>)
    data class Configuration(val output_dir: String): io.dropwizard.Configuration()

    class Application: io.dropwizard.Application<Configuration>() {
        private val logger = loggerFor<Application>()

        private var ingestorsJson: String? = null
        private var s3Bucket: String? = null
        private var s3Key: String? = null
        private var s3Secret: String? = null

        override fun getName(): String {
            return "bestiary-aggregator"
        }

        override fun bootstrapLogging() {}

        override fun initialize(bootstrap: Bootstrap<Configuration>) {
            logger.info("initialising...")

            ingestorsJson = System.getenv()["INGESTORS"]
            s3Bucket = System.getenv()["AMAZON_S3_BUCKET"]
            s3Key = System.getenv()["AMAZON_S3_KEY"]
            s3Secret = System.getenv()["AMAZON_S3_SECRET"]

            bootstrap.objectMapper.registerKotlinModule()

            bootstrap.configurationSourceProvider = SubstitutingSourceProvider(
                bootstrap.configurationSourceProvider,
                EnvironmentVariableSubstitutor()
            )
        }

        override fun run(configuration: Configuration, environment: Environment) {
            logger.info("hello, world")

            val ingestorsJson = this.ingestorsJson ?: throw RuntimeException("INGESTORS must be set")

            val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            val ingestorsAdapter = moshi.adapter(IngestorsConfig::class.java).lenient()
            val parsedIngestorsJson = try {
                ingestorsAdapter.fromJson(ingestorsJson) ?: throw RuntimeException("Ingestors was null")
            } catch (exception: Exception) {
                throw RuntimeException("Couldn't parse ingestors: $exception '''$ingestorsJson'''")
            }

            val ingestors = parsedIngestorsJson.ingestors
                .filter { it.type == "anilog" }
                .map { it.name to AnilogIngest(
                    it.baseUrl,
                    OkHttpClient()
                )
                }
                .toMap()

            logger.info("configured ingestors: $ingestors")

            val s3Bucket = this.s3Bucket
            val s3Key = this.s3Key
            val s3Secret = this.s3Secret
            if (s3Bucket == null || s3Key == null || s3Secret == null) {
                throw RuntimeException("Must set AMAZON_S3_BUCKET, AMAZON_S3_KEY, and AMAZON_S3_SECRET")
            }
            if (s3Bucket.isBlank() || s3Key.isBlank() || s3Secret.isBlank()) {
                throw RuntimeException("Amazon S3 bucket, key or secret were blank")
            }

            val s3Client = buildS3Client(s3Key, s3Secret)

            val ingestResource = IngestResource(
                IngestJobManager(
                    ingestors,
                    configuration.output_dir,
                    s3Bucket,
                    s3Client
                )
            )
            val ingestHealthCheck = IngestHealthCheck(ingestResource)

            environment.healthChecks().register("ingest", ingestHealthCheck)
            environment.jersey().register(ingestResource)

            val transformResource = TransformResource()
            val transformHealthCheck = TransformHealthCheck(transformResource)

            environment.healthChecks().register("transform", transformHealthCheck)
            environment.jersey().register(transformResource)
        }

        private fun buildS3Client(key: String, secret: String): AmazonS3 {
            val endpoint = AwsClientBuilder.EndpointConfiguration("https://ams3.digitaloceanspaces.com", "ams3")
            val credentials = AWSStaticCredentialsProvider(BasicAWSCredentials(key, secret))

            return AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(endpoint)
                .withCredentials(credentials)
                .build()
        }

    }


}