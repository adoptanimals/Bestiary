package animals.adopt.bestiary.aggregator.anilog.ingest

import animals.adopt.bestiary.aggregator.IIngestor
import animals.adopt.bestiary.aggregator.helper.Helper
import animals.adopt.bestiary.aggregator.helper.Helper.doWeHaveLatestAlready
import animals.adopt.bestiary.aggregator.helper.Regexes
import animals.adopt.bestiary.common.helper.loggerFor
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.services.s3.transfer.TransferManagerBuilder
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import io.reactivex.rxkotlin.blockingSubscribeBy
import io.reactivex.rxkotlin.toObservable
import okhttp3.OkHttpClient
import java.io.File


class AnilogIngest(private val baseUrl: String, private val client: OkHttpClient): IIngestor {

    val logger = loggerFor<AnilogIngest>()

    val mapper: XmlMapper = XmlMapper()
    val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    override val name = "anilog_v1"

    init {
        mapper.registerKotlinModule()
        mapper.setDefaultUseWrapper(false)
        mapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    }

    override fun ingest(jobDirectory: File, s3Bucket: String, s3Client: AmazonS3) {
        val exportFilename = "WebsiteExport.xml"
        val exportUrl = "$baseUrl/$exportFilename"

        val timestamp = "${System.currentTimeMillis() / 1000L}"
        val existingJobPrefix = "ingest/$name/${jobDirectory.name}/"

        if (!shouldDownloadNewVersion(s3Bucket, existingJobPrefix, s3Client, exportFilename, exportUrl)) {
            logger.info("Not grabbing new version because we already have the latest according to web metadata")
            return
        }

        val baseDir = File(jobDirectory, timestamp)
        Helper.clearAndRemakeDirectory(baseDir)

        val imagesDir = File(baseDir, "images/")
        Helper.clearAndRemakeDirectory(imagesDir)

        val expectedContentType = "application/xml"
        val sizeLimitBytes = 10L * Helper.bytesInMegabyte

        Helper.downloadFile(
            exportUrl,
            client,
            moshi,
            expectedContentType,
            sizeLimitBytes,
            baseDir,
            exportFilename
        )
            .map {
                val file: AnilogExport.Data = Helper.readXmlFile(mapper, it.file) ?: throw RuntimeException("Couldn't parse anilog export")
                val nonBlankAnimals = file.animals.map { it.image_id }.filterNot { it.isBlank() }
                if (nonBlankAnimals.isEmpty() || nonBlankAnimals.size > 100) {
                    throw RuntimeException("Unexpected number of animals to process - ${nonBlankAnimals.size}")
                } else {
                    return@map nonBlankAnimals
                }
            }
            .doOnNext { logger.info("There are ${it.size} animal images to download...") }
            .flatMap { it.toObservable() }
            .flatMap {
                val filename = "$it.jpg"
                if (!Regexes.files.test(filename)) {
                    throw RuntimeException("Bad filename for image id $it - $filename")
                }

                val url = "$baseUrl/$filename"
                val imageContentType = "image/jpeg"

                return@flatMap Helper.downloadFile(
                    url,
                    client,
                    moshi,
                    imageContentType,
                    sizeLimitBytes,
                    imagesDir,
                    filename
                )
            }
            .toList()
            .toObservable()
            .blockingSubscribeBy(onNext = {
                logger.info("Grabbed ${it.size} images")

                val newJobPrefix = "$existingJobPrefix$timestamp"

                val transferManager = TransferManagerBuilder.standard().withS3Client(s3Client).build()

                val upload = transferManager.uploadDirectory(s3Bucket, newJobPrefix, baseDir, true)

                logger.info("Uploading $newJobPrefix...")
                upload.waitForCompletion()

                logger.info("Upload complete")

            }, onError = {
                logger.error("Ended with error: ${it.localizedMessage}")
            })

            logger.info("Cleaning up...")
            if (baseDir.exists()) {
                try {
                    baseDir.deleteRecursively()
                } catch (exeption: Exception) {
                    logger.error("Tried to clear up ingest, but failed: ${exeption.localizedMessage}")
                }
            }
            logger.info("Done")
    }

    private fun shouldDownloadNewVersion(
        s3Bucket: String,
        existingJobPrefix: String,
        s3Client: AmazonS3,
        exportFilename: String,
        exportUrl: String
    ): Boolean {
        val existingDownloads = try {
            val request = ListObjectsRequest()
                .withBucketName(s3Bucket)
                .withPrefix(existingJobPrefix)
                .withDelimiter("/")

            s3Client.listObjects(request)
                .commonPrefixes
                .map {
                    it
                        .removePrefix(existingJobPrefix)
                        .removeSuffix("/")
                }
                .filter { Regexes.numeric.test(it) }
        } catch (exception: Exception) {
            null
        }

        val latestExistingTimestamp = existingDownloads?.sortedDescending()?.firstOrNull()
        if (latestExistingTimestamp != null) {
            val latestExistingPrefix = "$existingJobPrefix$latestExistingTimestamp"
            val haveLatestAlready = doWeHaveLatestAlready(
                client,
                s3Bucket,
                s3Client,
                moshi,
                latestExistingPrefix,
                exportFilename,
                exportUrl
            )
            if (haveLatestAlready) {
                return false
            }
        }

        return true
    }

    override fun toString(): String {
        return "AnilogIngest(baseUrl=$baseUrl)"
    }
}