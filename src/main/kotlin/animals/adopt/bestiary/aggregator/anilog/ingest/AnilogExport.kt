package animals.adopt.bestiary.aggregator.anilog.ingest

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

object AnilogExport {
    data class Data(val export_date: String, val animal_records: Int, @JacksonXmlProperty(localName = "animal") val animals: List<Animal>)
    data class Animal(val animalref: String,
                      val contact_number: String,
                      val species: Species,
                      val age_year: Int,
                      val age_month: Int,
                      val sex: Sex,
                      val breed: String,
                      val crossbreed: String,
                      val size: String,
                      val location: String,
                      val web_text: String,
                      val reserved: String,
                      val image_id: String,
                      val colour_list: String?)

    enum class Species {
        CAT, DOG
    }

    enum class Sex {
        MALE, FEMALE
    }
}