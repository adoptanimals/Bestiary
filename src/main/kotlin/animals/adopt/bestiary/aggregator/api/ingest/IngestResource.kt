package animals.adopt.bestiary.aggregator.api.ingest

import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/v1/ingest")
@Produces(MediaType.APPLICATION_JSON)
class IngestResource(private val jobs: IngestJobManager) {

    @GET
    @Path("/{jobName}")
    fun get(@PathParam("jobName") jobName: String): Response {
        val isRunning = jobs.isJobRunning(jobName)
        if (isRunning == null) {
            val message = "Job doesn't exist"
            throw WebApplicationException(message, Response.Status.NOT_FOUND)
        }

        if (isRunning) {
            val message = "Job already running"
            throw WebApplicationException(message, Response.Status.CONFLICT)
        }

        val ranJob = jobs.runJob(jobName)
        if (ranJob == null || !ranJob) {
            val message = "Job failed to run"
            throw WebApplicationException(message, Response.Status.INTERNAL_SERVER_ERROR)
        }

        return Response.status(200).build()
    }

}