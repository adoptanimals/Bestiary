package animals.adopt.bestiary.aggregator.api.transform

import animals.adopt.bestiary.common.helper.loggerFor
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/v1/transform")
@Produces(MediaType.APPLICATION_JSON)
class TransformResource {

    private val logger = loggerFor<TransformResource>()

    @GET
    @Path("/{jobName}/{id}")
    fun get(@PathParam("jobName") jobName: String, @PathParam("id") id: String): Response {
        throw WebApplicationException("no job with that associated id exists", Response.Status.NOT_FOUND)
    }

}