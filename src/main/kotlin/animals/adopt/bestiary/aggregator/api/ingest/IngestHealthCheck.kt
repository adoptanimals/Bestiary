package animals.adopt.bestiary.aggregator.api.ingest

import com.codahale.metrics.health.HealthCheck
import javax.ws.rs.WebApplicationException

class IngestHealthCheck(private val resource: IngestResource): HealthCheck() {

    override fun check(): Result {
        try {
            resource.get("nonexistent")
        } catch (exception: WebApplicationException) {
            if (exception.response.status == 404) {
                return Result.healthy()
            }
        } catch (exception: WebApplicationException) {
            return Result.unhealthy(exception)
        }

        return Result.unhealthy("Didn't get a 404 when we expected one")
    }

}