package animals.adopt.bestiary.aggregator.api.ingest

import animals.adopt.bestiary.aggregator.IIngestor
import animals.adopt.bestiary.common.helper.loggerFor
import com.amazonaws.services.s3.AmazonS3
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean

class IngestJobManager(private val ingestors: Map<String, IIngestor>, private val outputDirectory: String, private val s3Bucket: String, private val s3Client: AmazonS3) {

    private val logger =
        loggerFor<IngestJobManager>()

    private val jobsRunning =
        ConcurrentHashMap<String, AtomicBoolean>()

    init {
        ingestors.map {
            it.key to AtomicBoolean(false)
        }
        .toMap(jobsRunning)
    }

    fun isJobRunning(name: String): Boolean? {
        return jobsRunning[name]?.get()
    }

    fun runJob(name: String): Boolean? {
        val isRunning = isJobRunning(name) ?: return null
        if (isRunning) {
            return false
        }

        val ingestor = ingestors[name] ?: return null

        if (!runIngestorForJob(name, ingestor)) return null

        return true
    }

    private fun runIngestorForJob(name: String, ingestor: IIngestor): Boolean {
        jobsRunning[name]?.compareAndSet(false, true)

        Observable.just(name)
            .observeOn(Schedulers.io())
            .subscribeBy(onNext = {
                logger.info("running job: $name")
                val jobDir = prepareJobDirectory(name)
                if (jobDir != null) {
                    ingestor.ingest(jobDir, s3Bucket, s3Client)
                }
                jobsRunning[name]?.compareAndSet(true, false)
            }, onError = {
                logger.error("job failed to run: $it")
                jobsRunning[name]?.compareAndSet(true, false)
            })

        return true
    }

    private fun prepareJobDirectory(jobName: String): File? {
        val path = Paths.get("$outputDirectory/$jobName")
        return try {
            val jobDirectory = Files.createDirectory(path)
            logger.info("Made job directory: $jobName")
            jobDirectory.toFile()
        } catch (exception: java.nio.file.FileAlreadyExistsException) {
            logger.debug("Job directory for $jobName already exists")
            return path.toFile()
        } catch (exception: Exception) {
            logger.error("Failed to make job directory: $exception")
            return null
        }
    }

}