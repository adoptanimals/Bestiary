package animals.adopt.bestiary.aggregator.api.transform

import com.codahale.metrics.health.HealthCheck
import javax.ws.rs.WebApplicationException

class TransformHealthCheck(private val resource: TransformResource): HealthCheck() {

    override fun check(): Result {
        try {
            resource.get("nonexistent", "nonexistent")
        } catch (exception: WebApplicationException) {
            if (exception.response.status == 404) {
                return Result.healthy()
            }
        } catch (exception: WebApplicationException) {
            return Result.unhealthy(exception)
        }

        return Result.unhealthy("Didn't get a 404 when we expected one")
    }

}