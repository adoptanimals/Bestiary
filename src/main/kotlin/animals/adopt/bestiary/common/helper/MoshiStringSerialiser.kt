package animals.adopt.bestiary.common.helper

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.reflect.Type

interface IStringSerialiser<in T> {
    fun serialise(value: T): String?
}

interface IStringParser<out T> {
    fun parse(value: String): T?
}

inline fun <reified Type> Moshi.stringSerialiser(): IStringSerialiser<Type> {
    val adapter = this.adapter(Type::class.java).indent("  ")

    return makeSerialiser(adapter)
}

inline fun <reified Type> Moshi.stringParser(): IStringParser<Type> {
    val adapter = this.adapter(Type::class.java)

    return makeParser(adapter)
}

inline fun <reified JsonType> Moshi.stringSerialiser(type: Type, vararg parameters: Type): IStringSerialiser<JsonType> {
    val workaroundType = Types.newParameterizedType(type, *parameters)
    val adapter = this.adapter<JsonType>(workaroundType)

    return makeSerialiser(adapter)
}

fun <T> makeSerialiser(adapter: JsonAdapter<T>): IStringSerialiser<T> {
    return object : IStringSerialiser<T> {
        override fun serialise(value: T): String? {
            return try {
                adapter.toJson(value)
            } catch (exception: Exception) {
                return null
            }
        }
    }
}

fun <T> makeParser(adapter: JsonAdapter<T>): IStringParser<T> {
    return object : IStringParser<T> {
        override fun parse(value: String): T? {
            return try {
                adapter.fromJson(value)
            } catch (exception: Exception) {
                return null
            }
        }
    }
}