package animals.adopt.bestiary.common.helper

import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

object OkHttpExecuting {

    fun execute(request: Request, client: OkHttpClient): Observable<Response> {
        return Observable.fromCallable {
            client.newCall(request).execute()
        }
    }

}