#!/bin/bash

: "${CONTAINER_TAG?Need to set CONTAINER_TAG}"

find build/libs -name "Bestiary*all.jar" | head -n 1 | xargs -I '{}' \
  docker build -t adoptanimals/scheduler:$CONTAINER_TAG \
  --build-arg jar={} \
  --build-arg config=schedulerRun/scheduler.yaml \
  --file ci/scheduler/Dockerfile \
  .