#!/bin/bash

: "${CONTAINER_NAME?Need to set CONTAINER_NAME}"
: "${CONTAINER_TAG?Need to set CONTAINER_TAG}"
: "${AGGREGATOR_EDCH_URL?Need to set AGGREGATOR_EDCH_URL}"
: "${APP_PORT?Need to set APP_PORT}"
: "${ADMIN_PORT?Need to set ADMIN_PORT}"

docker run --name $CONTAINER_NAME \
  --detach \
  --env APP_PORT=$APP_PORT \
  --env ADMIN_PORT=$ADMIN_PORT \
  --env BINDHOST=0.0.0.0 \
  --env AGGREGATOR_EDCH_URL=$AGGREGATOR_EDCH_URL \
  adoptanimals/scheduler:$CONTAINER_TAG