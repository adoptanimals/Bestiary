#!/bin/bash
set -e
set -o pipefail

: "${DO_API_TOKEN?Need to set DO_API_TOKEN}"
: "${PRIVATE_KEY?Need to set PRIVATE_KEY}"

export ANSIBLE_HOST_KEY_CHECKING=False

ansible-playbook -u root \
    --private-key "${PRIVATE_KEY}" \
    --inventory-file=digital_ocean.py \
    -e 'ansible_python_interpreter=/usr/bin/python3' \
    deploy-docker.playbook.yaml