#!/bin/bash

: "${CONTAINER_NAME?Need to set CONTAINER_NAME}"
: "${INGESTORS?Need to set INGESTORS}"
: "${APP_PORT?Need to set APP_PORT}"
: "${ADMIN_PORT?Need to set ADMIN_PORT}"
: "${CONTAINER_TAG?Need to set CONTAINER_TAG}"
: "${AMAZON_S3_BUCKET?Need to set AMAZON_S3_BUCKET}"
: "${AMAZON_S3_KEY?Need to set AMAZON_S3_KEY}"
: "${AMAZON_S3_SECRET?Need to set AMAZON_S3_SECRET}"

docker run --name $CONTAINER_NAME \
  --detach \
  --env APP_PORT=$APP_PORT \
  --env ADMIN_PORT=$ADMIN_PORT \
  --env BINDHOST=0.0.0.0 \
  --env OUTPUT_DIR="." \
  --env INGESTORS=$INGESTORS \
  --env AMAZON_S3_BUCKET=$AMAZON_S3_BUCKET \
  --env AMAZON_S3_KEY=$AMAZON_S3_KEY \
  --env AMAZON_S3_SECRET=$AMAZON_S3_SECRET \
  --volume aggregatorRun:/output \
  adoptanimals/aggregator:$CONTAINER_TAG