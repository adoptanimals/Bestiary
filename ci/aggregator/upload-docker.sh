#!/bin/bash

: "${CONTAINER_TAG?Need to set CONTAINER_TAG}"
: "${DOCKERHUB_USER?Need to set DOCKERHUB_USER}"
: "${DOCKERHUB_PASSWORD?Need to set DOCKERHUB_PASSWORD}"

docker login --username=$DOCKERHUB_USER --password=$DOCKERHUB_PASSWORD
docker push adoptanimals/aggregator:${CONTAINER_TAG}